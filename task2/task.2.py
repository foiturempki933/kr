from os import name as os_name, system as os_system
from sys import stdin as sys_stdin
from time import sleep as time_sleep

os_system("cls" if os_name == "nt" else "clear")
earthall = []  # Все кадры
earthone = []  # Один кадр
earthreading = False  # Считывание
for line_r in sys_stdin:
    line = line_r.rstrip("\n")
    if line.startswith("```") == 1:
        if earthreading is False:  # Условие начала считывания.
            earthreading = True
        else: # Условие окончания считывания
            earthall.append(earthone.copy())  # Копии кадров в массив
            earthreading = False
            earthone = []

        continue

    if earthreading is True: # Добавление строки кадра.
        earthone.append(line)


try:
    while True:
        for earthone in earthall:
            for earthone_line in earthone:
                print("\u001b[32;1m", end="")  # Красим цвет текста в консоли
                print(earthone_line)
            time_sleep(0.7)
            os_system("cls" if os_name == "nt" else "clear")
except KeyboardInterrupt:
    pass

