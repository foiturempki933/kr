main = 42
element = 0

try:
    while True:
        try:
            element = float(input().rstrip())
        except ValueError:
            pass
        try:
            main += 1 / element
        except ZeroDivisionError:
            print('в строке 0')
            pass
except EOFError:
    print(f'Сумма ряда: {main}')
