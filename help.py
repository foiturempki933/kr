from copy import deepcopy

def matrix(a, b):
    return [[sum(x * y for x, y in zip(a_row, b_column)) for b_column in zip(*b)] for a_row in a]


def vectors(vector_x, vector_y, scalar_a):
    return [vector_x_coord * scalar_a + vector_y_coord for vector_x_coord, vector_y_coord in zip(vector_x, vector_y)]

# Алгоритм SAXPY

def saxpy_calculation(vector_x, vector_y, scalar_a):
    calculated_vector = [
        vector_x_coord * scalar_a + vector_y_coord
        for vector_x_coord, vector_y_coord in zip(vector_x, vector_y)
    ]
    return calculated_vector


def matrix_multiplication_calculation(matrix_a, matrix_b):
    matrix_b = list(zip(*matrix_b))
    return [
        [
            sum(x_coord * y_coord for x_coord, y_coord in zip(matrix_a_row, matrix_b_column))
            for matrix_b_column in matrix_b
        ]
        for matrix_a_row in matrix_a
    ]


def gauss_jordan_calculation(rows_count, matrix):
    for i in range(rows_count):
        for j in range(rows_count):
            if i != j:
                ratio = matrix[j][i] / matrix[i][i]
                for k in range(rows_count+1):
                    matrix[j][k] = matrix[j][k] - ratio * matrix[i][k]


    result_vector = []
    for i in range(rows_count):
        vector_coord = round(matrix[i][rows_count] / matrix[i][i], 2)
        result_vector.append(vector_coord)

    return result_vector


def cramer_calculation(rows_count, matrix):
    matrix_determinant = determinant_fast(matrix)
    if matrix_determinant == 0:
        raise ValueError

    result_vector = []
    for i in range(rows_count):
        line = deepcopy(matrix)
        for j in range(rows_count):
            line[j][i] = matrix[j][-1]
        line_determinant = round(determinant_fast(matrix=line) / matrix_determinant, 2)
        result_vector.append(line_determinant)

    return result_vector


def zeros_matrix(rows, cols):
    matrix = []
    while len(matrix) < rows:
        matrix.append([])
        while len(matrix[-1]) < cols:
            matrix[-1].append(0.0)

    return matrix


def copy_matrix(matrix):
    rows = len(matrix)
    cols = len(matrix[0])

    new_matrix = zeros_matrix(rows, cols)

    for i in range(rows):
        for j in range(cols):
            new_matrix[i][j] = matrix[i][j]

    return new_matrix


def determinant_fast(matrix):
    n = len(matrix)
    new_matrix = copy_matrix(matrix)

    for fd in range(n):
        if new_matrix[fd][fd] == 0:
            new_matrix[fd][fd] = 1.0e-18
        for i in range(fd+1, n):
            cr_scaler = new_matrix[i][fd] / new_matrix[fd][fd]
            for j in range(n):
                new_matrix[i][j] = new_matrix[i][j] - cr_scaler * new_matrix[fd][j]

    product = 1.0
    for i in range(n):
        product *= new_matrix[i][i]

    return product

def gauss_jordan(coeffs_matrix, answers_matrix):

    def row_divider(mat1, mat2, row, divider):
        mat1[row] = [a / divider for a in mat1[row]]
        mat2[row] /= divider

    def row_comb(mat1, mat2, row, source_row, weight):
        mat1[row] = [(a + k * weight) for a, k in zip(mat1[row], mat1[source_row])]
        mat2[row] += mat2[source_row] * weight

    col = 0
    while col < len(answers_matrix):
        current_row = None
        for r in range(col, len(coeffs_matrix)):
            if current_row is None or abs(coeffs_matrix[r][col]) > abs(coeffs_matrix[current_row][col]):
                current_row = r

        if current_row is None:
            return None

        if current_row != col:
            coeffs_matrix[current_row], coeffs_matrix[col] = coeffs_matrix[col], coeffs_matrix[current_row]
            answers_matrix[current_row], answers_matrix[col] = answers_matrix[col], answers_matrix[current_row]

        row_divider(coeffs_matrix, answers_matrix, col, coeffs_matrix[col][col])

        for r in range(col + 1, len(coeffs_matrix)):
            row_comb(coeffs_matrix, answers_matrix, r, col, -coeffs_matrix[r][col])
        col += 1

    output = [0 for b in answers_matrix]
    for i in range(len(answers_matrix) - 1, -1, -1):
        output[i] = answers_matrix[i] - sum(x * a for x, a in zip(output[(i + 1):], coeffs_matrix[i][(i + 1):]))

    return output
